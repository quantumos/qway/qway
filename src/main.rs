extern crate wlroots_sys;
extern crate wlroots;
extern crate libc;
#[macro_use] extern crate wayland_sys;
extern crate xkbcommon;

use std::mem::transmute;
use wayland_sys::server::*;
use xkbcommon::xkb;
use wlroots::input::Device;
use wlroots::output::Output;
use wlroots::{
    resource::Resource,
    display::Display,
    backend::Backend,
};

//struct State {
    //display: wayland_sys::wl_display,
    //new_output: wayland_sys::wl_listener,
//}
//extern crate wayland_server;

//use wayland_sys::ffi_dispatch;
//use wlroots2::log::{Logger, LogLevel};
//use std::ptr;

//extern "C" fn new_output(_listener: *mut wl_listener, data: *mut libc::c_void) {
    //println!("New output!!!");

    //unsafe {
        //let output = transmute::<*mut libc::c_void, *mut wlroots_sys::wlr_output>(data);
    //}
//}

//extern "C" fn new_input(_listener: *mut wl_listener, data: *mut libc::c_void) {
    //println!("New input!!!");

    //unsafe {
        //let device = wlroots::input::Device::from_void_data(data);
        //println!("{:?}", device.get_type().unwrap());

        //match device.get_type().unwrap() {
            //wlroots::input::Type::Keyboard => {
                //let keyboard = device.get_keyboard().unwrap();

                //let context = xkb::Context::new(0);
                //// keeping params empty chooses default values
                //let keymap = xkb::Keymap::new_from_names(&context,
                    //"", "", "", "", None, 0).unwrap();
                //keyboard.set_keymap(&keymap);
            //},
            //_ => {}
        //}
    //}
//}

fn main() {
    wlroots::log::Logger::init(wlroots::log::LogLevel::Debug, None);

    let mut display = Display::new();
    let mut backend = Backend::new(&mut display, None);

    backend.register_callbacks();
    backend.start();

    std::thread::sleep(std::time::Duration::from_millis(10000))
    //let display = wlroots::display::Di[>splay::create();<]
    //let backend = wlroots::backend::Backend::create(&display, None);

    //backend.set_input_create_callback(|device| {
        //println!("New input!!!");

        //println!("{:?}", device.get_type().unwrap());

        //match device.get_type().unwrap() {
            //wlroots::input::Type::Keyboard => {
                //let keyboard = device.get_keyboard().unwrap();

                //let context = xkb::Context::new(0);
                //// keeping params empty chooses default values
                //let keymap = xkb::Keymap::new_from_names(&context,
                    //"", "", "", "", None, 0).unwrap();
                //keyboard.set_keymap(&keymap);
            //},
            //_ => {}
        //}
    //});

    //backend.set_output_create_callback(|output| {
        //println!("New output!!!!~~~~");

        //output.set_output_frame_callback(|output| {
            //println!("new frame");
        //});
    //});

    ////backend.set_new_output_callback(new_output);

    //if !backend.start() {
        //println!("Failed to start backend");
        //std::process::exit(1);
    //}

//[>    d<]isplay.run();
}
